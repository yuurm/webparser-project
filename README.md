An application that allows you to parse any HTML page and provides statistics on the number of unique words.
Application Requirements
1 As input, the application accepts a string with the address
of a web page. Example of an input string: https://www.microsoft.com/
2 The application splits the page text into separate words using
a list of separators.
Example of a list:
{' ', ',', '.', '! ', '?','"', ';', ':', '[', ']', '(', ')', '\ n', '\r', '\t'}
3 As a result of the work, the user should get statistics on
the number of unique words in the text.
Example:
DEVELOPMENT -1
SOFTWARE - 2
SECURITY - 4
4 When developing, it is allowed to use third-party libraries.
5 The application is written in accordance with the principles of OOP
6 The application is written in the language of the chosen direction (Java)
What we like about the app
It is enough to enroll if your application works and
has more than one class, but it is worth considering the limited number of places, so
a good bonus will be:
1 Good code style, close to the generally accepted standards
of code formatting
2 Using design patterns
3 Logging errors to a file
4 Saving statistics to the database.
5 The possibility of project extensibility and multi-level architecture
6 Tests